﻿create database moremoney;

create table users (
    userId INT NOT NULL PRIMARY KEY,
    name VARCHAR(255) DEFAULT NULL,
    email VARCHAR(255) DEFAULT NULL,
    password VARCHAR(255) DEFAULT NULL
    );

insert into users values (1,'brd','2dl@tut.by','1a1dc91c907325c69271ddf0c944bc72');
insert into users values (2,'valera','valera@tut.by','81dc9bdb52d04dc20036dbd8313ed055');
insert into users values (3,'orlova','orlova@tut.by', '00ba8dd78ef46cf7d20ff3aa97e02bae');
insert into users values (4,'igor','igor@tut.by', 'dd97813dd40be87559aaefed642c3fbb');
insert into users values (5,'serega','serega@tut.by', 'f0544a6185d7fa2c883e106f6efad5ff');

create table account_types (
    account_types INT NOT NULL PRIMARY KEY,
    description VARCHAR(255) DEFAULT NULL
    );

insert into account_types values (1, 'cash');
insert into account_types values (2, 'credit card');
insert into account_types values (3, 'debet card');
insert into account_types values (4, 'emoney');


create table accounts (
    accountId INT NOT NULL PRIMARY KEY,
    userId INT DEFAULT NULL,
    accountName VARCHAR(255) DEFAULT NULL,
    balance INT DEFAULT NULL,
    account_type_id INT DEFAULT NULL, 
    accountNumber VARCHAR(255) DEFAULT NULL
    );

insert into accounts values (1, 1, 'Втб-24', 5200, 3, '3400121145782574');
insert into accounts values (2, 1, 'копилка',1200, 1, null);
insert into accounts values (3, 2, 'сбербанк',11200, 3, '3400123345782590');
insert into accounts values (4, 3, 'веб-мани',1100, 4, 'Z4441230018078');
insert into accounts values (5, 4, 'киви',100, 4, '932147845147');
insert into accounts values (6, 5, 'наличка',10600, 1, null);


create table transaction_types (
    transactionTypeId INT NOT NULL PRIMARY KEY,
    description VARCHAR(255) DEFAULT NULL
    );

insert into  transaction_types values (01, 'put');
insert into  transaction_types values (02, 'transfer');
insert into  transaction_types values (03, 'withdraw'); 


create table transaction_categories (
    transactionCategoryId INT NOT NULL PRIMARY KEY,
    description VARCHAR(255) DEFAULT NULL,
    transactionTypeId INT DEFAULT NULL
    );
    
insert into  transaction_categories values (01, 'food', 03);
insert into  transaction_categories values (02, 'fuel', 03);
insert into  transaction_categories values (03, 'travel', 03);
insert into  transaction_categories values (04, 'salary', 01);
insert into  transaction_categories values (05, 'sport club', 03);

create table transactions (
    transaction_id INT NOT NULL PRIMARY KEY,
    date timestamp,
    account_id1 INT,
    account_id2 INT, 
    transactionCategoryId INT,
    sum INT,
    full_description VARCHAR(255) 
    );

insert into transactions values (01, '2018-08-01 12:00', 01, null, 04, 30000, 'salary payments july-18');
insert into transactions values (02, '2018-08-01 11:00', 03, null, 04, 24000, 'salary payments july-18');
insert into transactions values (03, '2018-08-01 14:00', 01, null, 02, 2000, 'ai-92');

select  transactions.date as "Data",
        users.name as "User name", 
        transactions.sum as "Value", 
        transaction_types.description as "Type" , 
        transaction_categories.description as "Description",
        transactions.full_description as "Transfer description"
        
from transactions join accounts on accounts.accountId = transactions.account_id1 join users on users.userId = accounts.userId
join transaction_categories on  transactions.transactionCategoryId = transaction_categories.transactionCategoryId
join transaction_types on transaction_types.transactionTypeId = transaction_categories.transactionTypeId
order by  transactions.date