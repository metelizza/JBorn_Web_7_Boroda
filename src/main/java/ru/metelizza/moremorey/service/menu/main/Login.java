package ru.metelizza.moremorey.service.menu.main;

import ru.metelizza.moremorey.dao.ServiceFactory;
import ru.metelizza.moremorey.models.User;
import ru.metelizza.moremorey.service.AuthService;
import ru.metelizza.moremorey.view.MessageView;
import ru.metelizza.moremorey.view.menu.AccountMenuView;
import ru.metelizza.moremorey.view.menu.MainMenuView;

import java.util.Scanner;

public class Login {

    public static User tryLogin() {
        Scanner in = new Scanner(System.in);

        MessageView.view("Enter your login");
        String login = in.nextLine();
        MessageView.view("Enter your password");
        String password = in.nextLine();

        User user = ServiceFactory.getAuthService().auth(login, password);
        if (user != null && user.getStatus() == true) {
            MessageView.viewGreen("Success!");
            AccountMenuView.view();
            return user;
        } else {
            MessageView.viewPurple("It's not you!");
            MainMenuView.view();
            return null;
        }
    }
}
