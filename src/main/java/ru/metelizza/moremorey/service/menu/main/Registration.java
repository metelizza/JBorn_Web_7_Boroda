package ru.metelizza.moremorey.service.menu.main;

import ru.metelizza.moremorey.dao.DaoFactory;
import ru.metelizza.moremorey.dao.ServiceFactory;
import ru.metelizza.moremorey.dao.UserDao;
import ru.metelizza.moremorey.service.dto.UserDto;
import ru.metelizza.moremorey.view.MessageView;

import java.util.Scanner;

public class Registration {
    public static void tryRegistration() {
        Scanner in = new Scanner(System.in);

        MessageView.view("Enter email");
        String eMail = in.nextLine();

        UserDao userDao = DaoFactory.getUserDao();
        UserDto userDto = userDao.findByEmail(eMail);

        if (userDto == null) {
            MessageView.view("Enter your password");
            String password = in.nextLine();

            MessageView.view("Enter your name");
            String name = in.nextLine();

            String hashPassword = ServiceFactory.getDigitsService().hash(password);

            userDto = new UserDto();
            userDto.setEmail(eMail);
            userDto.setName(name);
            userDto.setPassword(hashPassword);

            userDto = userDao.insert(userDto);

        } else {
            MessageView.viewPurple("Error! You can't use this email!");
        }
    }
}
