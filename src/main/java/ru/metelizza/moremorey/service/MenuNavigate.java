package ru.metelizza.moremorey.service;

import ru.metelizza.moremorey.dao.*;
import ru.metelizza.moremorey.models.Account;
import ru.metelizza.moremorey.models.User;
import ru.metelizza.moremorey.service.dto.AccountDto;
import ru.metelizza.moremorey.service.dto.BankAccountDto;
import ru.metelizza.moremorey.service.dto.TransactionCategoriesDto;
import ru.metelizza.moremorey.service.menu.main.Registration;
import ru.metelizza.moremorey.view.*;
import ru.metelizza.moremorey.view.menu.AccountMenuView;
import ru.metelizza.moremorey.view.menu.BankAccountTypesMenu;
import ru.metelizza.moremorey.view.menu.MainMenuView;
import ru.metelizza.moremorey.view.menu.TransactionCategoriesMenu;

import java.util.List;
import java.util.Scanner;

import static ru.metelizza.moremorey.service.menu.main.Login.tryLogin;

public class MenuNavigate {
    static User currentUser = null;

    public static void Menu() {
        MainMenuView.view();

        boolean running = true;
        while (running) {
            Scanner in = new Scanner(System.in);
            String menuItem = in.nextLine();
            switch (menuItem) {
                default:
                    MessageView.view("You choose: " + menuItem);
                    MainMenuView.view();
                    break;
                case "1":
                    currentUser = tryLogin();
                    if (currentUser != null) {
                        AccountMenu();
                    }
                    break;

                case "2":
                    UsersView.viewName(DaoFactory.getUserDao().getAllUsers());
                    MainMenuView.view();
                    break;

                case "3":
                    Registration.tryRegistration();
                    MainMenuView.view();
                    break;

                case "4":
                    BankAccountTypesMenu.view();
                    BankAccountMenu();
                    break;

                case "5":
                    TransactionCategoriesMenu.view();
                    TransactionCategoriesMenu();
                    break;


                case "h":
                    MainMenuView.view();
                    break;

                case "0":
                    running = false;
            }
        }
    }

    public static void AccountMenu() {

        boolean running = true;
        while (running) {
            Scanner in = new Scanner(System.in);
            String menuItem = in.nextLine();
            List<Account> accounts = DaoFactory.getAccountDAO().getListOfUserAccounts(currentUser.getId());
            switch (menuItem) {
                default:
                    MessageView.view("You choose: " + menuItem);
                    AccountMenuView.view();
                    break;
                case "1":
                    AccountsView.view(accounts, currentUser);
                    AccountMenuView.view();
                    break;

                case "2":

                    if (accounts.size() < 5) {

                        int userId = currentUser.getId();

                        MessageView.view("Enter account name");
                        in = new Scanner(System.in);
                        String accountName = in.nextLine();

                        MessageView.view("Enter balance:");
                        in = new Scanner(System.in);
                        int balance = Integer.parseInt(in.nextLine());

                        MessageView.view("Enter account type: (1=cash,2=credit card,3=debet card)");
                        in = new Scanner(System.in);
                        int accountType = Integer.parseInt(in.nextLine());

                        AccountDao accountDao = DaoFactory.getAccountDAO();
                        AccountDto accountDto = new AccountDto();
                        accountDto.setAccountName(accountName);
                        accountDto.setUserId(userId);
                        accountDto.setAccountType(accountType);
                        accountDto.setBalance(balance);

                        accountDao.insert(accountDto);
                        AccountMenuView.view();
                        break;
                    } else MessageView.view("You have max (5) count!");
                    AccountMenuView.view();
                    break;
                case "0":
                    MainMenuView.view();
                    running = false;
            }
        }
    }

    public static void BankAccountMenu() {
        boolean running = true;
        while (running) {
            Scanner in = new Scanner(System.in);
            String menuItem = in.nextLine();
            switch (menuItem) {
                default:
                    MessageView.view("You choose: " + menuItem);
                    BankAccountTypesMenu.view();
                    break;
                case "1":
                    List<BankAccountDto> bankAccountsTypes = BankAccountDao.getListOfBankAccountTypes();
                    BankAccountsView.view(bankAccountsTypes);
                    BankAccountTypesMenu.view();
                    break;

                case "2":
                    MessageView.view("Enter description");
                    in = new Scanner(System.in);
                    String description = in.nextLine();
                    BankAccountDao bankAccountDao = DaoFactory.getBankAccountDao();
                    BankAccountDto bankAccountDto = new BankAccountDto();
                    bankAccountDto.setDescription(description);
                    bankAccountDto = bankAccountDao.insert(bankAccountDto);

                case "0":
                    MainMenuView.view();
                    running = false;
            }
        }
    }

    public static void TransactionCategoriesMenu() {
        boolean running = true;
        while (running) {
            Scanner in = new Scanner(System.in);
            String menuItem = in.nextLine();
            switch (menuItem) {
                default:
                    MessageView.view("You choose: " + menuItem);
                    BankAccountTypesMenu.view();
                    break;
                case "1":
                    List<TransactionCategoriesDto> transactionCategories = TransactionsCategoriesDao.getListOfTransactionCategories();
                    TransactionCategoriesView.view(transactionCategories);
                    TransactionCategoriesMenu.view();
                    break;

                case "2":
                    MessageView.view("Enter description");
                    in = new Scanner(System.in);
                    String description = in.nextLine();
                    MessageView.view("Enter type: (1=put,2=transfer,3=withdraw)");
                    in = new Scanner(System.in);
                    int type = Integer.parseInt(in.nextLine());

                    TransactionsCategoriesDao transactionsCategoriesDao = new TransactionsCategoriesDao();
                    TransactionCategoriesDto transactionCategoriesDto = new TransactionCategoriesDto();
                    transactionCategoriesDto.setDescription(description);
                    transactionCategoriesDto.setTransactionCategoryId(type);
                    transactionCategoriesDto = transactionsCategoriesDao.insert(transactionCategoriesDto);

                case "0":
                    MainMenuView.view();
                    running = false;
            }
        }
    }
}
