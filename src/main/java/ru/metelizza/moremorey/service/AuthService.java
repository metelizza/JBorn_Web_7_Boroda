package ru.metelizza.moremorey.service;

import ru.metelizza.moremorey.dao.DaoFactory;
import ru.metelizza.moremorey.dao.UserDao;
import ru.metelizza.moremorey.models.User;
import ru.metelizza.moremorey.service.dto.UserDto;

public class AuthService {

    private final UserDao userDao;
    private final DigitsService digitsService;

    public AuthService(UserDao userDao, DigitsService digitsService) {
        this.userDao = userDao;
        this.digitsService = digitsService;
    }

    public User auth(String eMail, String passwordHash) {
        UserDao userDao = DaoFactory.getUserDao();
        UserDto userDto = userDao.findByEmail(eMail);
        User user = new User();
        user.setPassword(userDto.getPassword());
        user.setEmail(userDto.getEmail());
        user.setName(userDto.getName());
        user.setStatus(userDto.getStatus());
        user.setId(userDto.getId());

        if (user != null) {
            String password = user.getPassword();
            String hashPassword = digitsService.hash(passwordHash);
            if (password.equals(hashPassword)) {
                user.setStatus(true);
            } else {
                user.setStatus(false);
            }
        } else {
            return null;
        }
        return user;
    }
}
