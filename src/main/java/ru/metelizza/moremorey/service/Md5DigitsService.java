package ru.metelizza.moremorey.service;

import static org.apache.commons.codec.digest.DigestUtils.md5Hex;

public class Md5DigitsService implements DigitsService {

    public String hash(String pass) {
        return md5Hex(pass);
    }
}
