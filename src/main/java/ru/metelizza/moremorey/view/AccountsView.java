package ru.metelizza.moremorey.view;

import ru.metelizza.moremorey.models.Account;
import ru.metelizza.moremorey.models.User;

import java.util.List;

public class AccountsView {

    public static void view(List<Account> accounts, User user) {
        System.out.println("User '" + user.getName() + "' have next accounts:");
        for (Account account : accounts) {
            System.out.println("Account name: " + account.getAccountName()
                    + '\t' + "  | balance: " + Integer.toString(account.getBalance()));
        }
    }
}
