package ru.metelizza.moremorey.view;

import ru.metelizza.moremorey.service.dto.BankAccountDto;

import java.util.List;

public class BankAccountsView {

    public static void view(List<BankAccountDto> accounts) {
        for (BankAccountDto bankAccountDto : accounts) {
            System.out.println("ID: " + Integer.toString(bankAccountDto.getId())
                    + '\t' + "| description: " + bankAccountDto.getDescription());
        }
    }
}
