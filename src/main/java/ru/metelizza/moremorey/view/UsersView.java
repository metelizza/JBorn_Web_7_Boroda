package ru.metelizza.moremorey.view;

import ru.metelizza.moremorey.service.dto.UserDto;

import java.util.List;

public class UsersView {

    public static void viewName(List<UserDto> users) {
        for (UserDto user : users) {
            System.out.println("USER ID = " + Integer.toString(user.getId()) +
                    '\t' + " | USER NAME = " + user.getName());
        }
    }
}
