package ru.metelizza.moremorey.view;

public class MessageView {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    public static void view(String string) {
        System.out.println(string);
    }

    public static void viewPurple(String string) {
        System.out.println(ANSI_PURPLE + string + ANSI_RESET);
    }

    public static void viewGreen(String string) {
        System.out.println(ANSI_GREEN + string + ANSI_RESET);
    }
}
