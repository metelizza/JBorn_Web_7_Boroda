package ru.metelizza.moremorey.view.menu;

import ru.metelizza.moremorey.view.MessageView;

public class AccountMenuView {
        public static void view() {
            MessageView.viewGreen("***********************************************");
            MessageView.viewGreen("You are in login submenu!");
            MessageView.view("'1' for view all accounts");
            MessageView.view("'2' for add new account");
            MessageView.view("'0' for exit");
            MessageView.view("'h' for help");
            MessageView.viewGreen("***********************************************");
        }
    }
