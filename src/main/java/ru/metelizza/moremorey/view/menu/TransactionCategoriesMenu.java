package ru.metelizza.moremorey.view.menu;

import ru.metelizza.moremorey.view.MessageView;

public class TransactionCategoriesMenu {
    public static void view() {
        MessageView.viewGreen("***********************************************");
        MessageView.viewGreen("You are in transactions categories type submenu!");
        MessageView.view("'1' for view all types");
        MessageView.view("'2' for add new type");
        MessageView.view("'0' for exit");
        MessageView.view("'h' for help");
        MessageView.viewGreen("***********************************************");
    }
}
