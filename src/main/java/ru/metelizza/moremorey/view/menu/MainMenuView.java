package ru.metelizza.moremorey.view.menu;

import ru.metelizza.moremorey.view.MessageView;

public class MainMenuView {
    public static void view() {
        MessageView.viewGreen("***********************************************");
        MessageView.view("Welcome!");
        MessageView.view("'1' for login (for test use '2dl@tut.by:pass')");
        MessageView.view("'2' for view all users");
        MessageView.view("'3' for register new user");
        MessageView.view("'4' for edit/view types of bank accounts");
        MessageView.view("'5' for edit/view transactions categories");
        MessageView.view("'0' for exit");
        MessageView.view("'h' for help");
        MessageView.viewGreen("***********************************************");
    }
}