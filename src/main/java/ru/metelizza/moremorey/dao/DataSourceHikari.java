package ru.metelizza.moremorey.dao;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;

public class DataSourceHikari {
    private static DataSource hikariDataSourceHikariPool;

    public static DataSource getDataSource() {
        if (hikariDataSourceHikariPool == null) {
            HikariConfig config = new HikariConfig();

            /*
            -DdbUrl=jdbc:postgresql://localhost:5432/postgres
            -DdbLogin=postgres
            -DdbPassword=postgres
            */

            config.setJdbcUrl(System.getProperty("dbUrl"));
            config.setUsername(System.getProperty("dbLogin"));
            config.setPassword(System.getProperty("dbPassword"));

            hikariDataSourceHikariPool = new HikariDataSource(config);
        }
        return hikariDataSourceHikariPool;
    }
}
