package ru.metelizza.moremorey.dao;

public class DaoFactory {
    private static UserDao userDao;

    public static UserDao getUserDao() {
        if (userDao == null) {
            userDao = new UserDao();
        }
        return userDao;
    }
    private static AccountDao accountDAO;

    public static AccountDao getAccountDAO() {
        if (accountDAO == null) {
            accountDAO = new AccountDao(DataSourceHikari.getDataSource());
        }
        return accountDAO;
    }

    private static BankAccountDao bankAccountDao;

    public static BankAccountDao getBankAccountDao() {
        if (bankAccountDao == null) {
            bankAccountDao = new BankAccountDao(DataSourceHikari.getDataSource());
        }
        return bankAccountDao;
    }

}
