

package ru.metelizza.moremorey.dao;

import ru.metelizza.moremorey.service.dto.UserDto;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDao implements Dao<UserDto, Integer> {
    @Override
    public UserDto findById(Integer id) {
        UserDto user = null;
        DataSource dataSource = DataSourceHikari.getDataSource();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement psm = connection.prepareStatement("select * from users where user_id = ?");
            psm.setInt(1, id);
            ResultSet resultSet = psm.executeQuery();
            while (resultSet.next()) {
                user = new UserDto();
                user.setEmail(resultSet.getString("email"));
                user.setId(resultSet.getInt("user_id"));
                user.setName(resultSet.getString("name"));
                user.setPassword(resultSet.getString("password"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return user;
    }

    public UserDto insert(UserDto user) {
        UserDto userDto = new UserDto();
        DataSource dataSource = DataSourceHikari.getDataSource();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement psm = connection.prepareStatement("insert into users (name, email, password) " + "values (?, ?, ?) ", Statement.RETURN_GENERATED_KEYS);
            psm.setString(1, user.getName());
            psm.setString(2, user.getEmail());
            psm.setString(3, user.getPassword());
            psm.execute();
            ResultSet rs = psm.getGeneratedKeys();
            if (rs.next()) {
                userDto.setId((int) rs.getLong(1));
            }
        } catch (SQLException e1) {
            e1.printStackTrace();
            throw new RuntimeException(e1);
        }
        return userDto;
    }

    @Override
    public UserDto update(UserDto userDto) {
        return null;
    }

    @Override
    public boolean delete(Integer integer) {
        return false;
    }

    public UserDto findByName(String name) {
        UserDto user = null;
        DataSource dataSource = DataSourceHikari.getDataSource();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement psm = connection.prepareStatement("select * from users where name = ?");
            psm.setString(1, name);
            ResultSet resultSet = psm.executeQuery();
            while (resultSet.next()) {
                user = new UserDto();
                user.setEmail(resultSet.getString("email"));
                user.setId(resultSet.getInt("user_id"));
                user.setName(resultSet.getString("name"));
                user.setPassword(resultSet.getString("password"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return user;
    }

    public UserDto findByEmail(String eMail) {
        UserDto user = null;
        DataSource dataSource = DataSourceHikari.getDataSource();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement psm = connection.prepareStatement("select * from users where email = ?");
            psm.setString(1, eMail);
            ResultSet resultSet = psm.executeQuery();
            while (resultSet.next()) {
                user = new UserDto();
                user.setEmail(resultSet.getString("email"));
                user.setId(resultSet.getInt("user_id"));
                user.setName(resultSet.getString("name"));
                user.setPassword(resultSet.getString("password"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return user;
    }

    public static List<UserDto> getAllUsers() {
        String SQL_QUERY = "select * from users";
        List<UserDto> users = null;
        DataSource dataSource = DataSourceHikari.getDataSource();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement pst = connection.prepareStatement(SQL_QUERY);
            ResultSet rs = pst.executeQuery();
            users = new ArrayList<>();
            UserDto user;
            while (rs.next()) {
                user = new UserDto();
                user.setEmail(rs.getString("email"));
                user.setId(rs.getInt("user_id"));
                user.setName(rs.getString("name"));
                user.setPassword(rs.getString("password"));
                users.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return users;
    }

    @Override
    public List<UserDto> findByAll() {
        return null;
    }
}

