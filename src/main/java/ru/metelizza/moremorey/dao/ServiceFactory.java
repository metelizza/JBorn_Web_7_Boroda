package ru.metelizza.moremorey.dao;

import ru.metelizza.moremorey.service.AuthService;
import ru.metelizza.moremorey.service.DigitsService;
import ru.metelizza.moremorey.service.Md5DigitsService;

public class ServiceFactory {
    private static AuthService authService;
    public static AuthService getAuthService() {
        if (authService==null) {
            authService = new AuthService(DaoFactory.getUserDao(), getDigitsService());
        }
        return authService;
    }

    private static DigitsService digitsService;

    public static DigitsService getDigitsService() {
        if (digitsService == null) {
            digitsService = new Md5DigitsService();
        }
        return digitsService;
    }
}
