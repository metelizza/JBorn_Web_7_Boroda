package ru.metelizza.moremorey.dao;

import ru.metelizza.moremorey.service.dto.TransactionCategoriesDto;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TransactionsCategoriesDao implements Dao <TransactionCategoriesDto, Integer>  {
    @Override
    public TransactionCategoriesDto findById(Integer integer) {
        return null;
    }

    @Override
    public List<TransactionCategoriesDto> findByAll() {
        return null;
    }

    @Override
    public TransactionCategoriesDto insert(TransactionCategoriesDto transactionCategoriesDto) {
            DataSource dataSource = DataSourceHikari.getDataSource();
            try (Connection connection = dataSource.getConnection()) {
                PreparedStatement psm = connection.prepareStatement("insert into transaction_categories (description, transaction_type_id) " +
                        "values (?,?)", Statement.RETURN_GENERATED_KEYS);
                psm.setString(1, transactionCategoriesDto.getDescription());
                psm.setInt(2, transactionCategoriesDto.getTransactionCategoryId());
                psm.execute();
                ResultSet rs = psm.getGeneratedKeys();
                if (rs.next()) {
                }
            } catch (SQLException e1) {
                e1.printStackTrace();
                throw new RuntimeException(e1);
            }

            return transactionCategoriesDto;
        }

    @Override
    public TransactionCategoriesDto update(TransactionCategoriesDto transactionCategoriesDto) {
        return null;
    }

    @Override
    public boolean delete(Integer integer) {
        return false;
    }

    public static List<TransactionCategoriesDto> getListOfTransactionCategories() {
        List<TransactionCategoriesDto> accounts = new ArrayList<>();
        TransactionCategoriesDto transactionCategoriesDto = null;
        DataSource dataSource = DataSourceHikari.getDataSource();
        try (Connection connection = dataSource.getConnection()){
            PreparedStatement psm = connection.prepareStatement("select * from transaction_categories");
            ResultSet resultSet = psm.executeQuery();
            while (resultSet.next()) {
                transactionCategoriesDto = new TransactionCategoriesDto();
                transactionCategoriesDto.setTransactionCategoryId(resultSet.getInt("transaction_category_id"));
                transactionCategoriesDto.setDescription(resultSet.getString("description"));
                transactionCategoriesDto.setTransactionTypeId(resultSet.getInt("transaction_type_id"));
                accounts.add(transactionCategoriesDto);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return accounts;
    }
}
