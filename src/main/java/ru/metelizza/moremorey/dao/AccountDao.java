package ru.metelizza.moremorey.dao;

import ru.metelizza.moremorey.models.Account;
import ru.metelizza.moremorey.service.dto.AccountDto;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AccountDao implements Dao<AccountDto, Integer> {

    private final DataSource dataSource;

    public AccountDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public AccountDto findById(Integer id) {
        return null;
    }

    @Override
    public AccountDto insert(AccountDto accountDto) {

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement psm = connection.prepareStatement("insert into accounts " +
                    "(user_id, account_name, balance, account_type_id) " +
                    "values (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            psm.setInt(1, accountDto.getUserId());
            psm.setString(2, accountDto.getAccountName());
            psm.setInt(3, accountDto.getBalance());
            psm.setInt(4, accountDto.getAccountType());
            psm.execute();
            ResultSet rs = psm.getGeneratedKeys();
            if (rs.next()) {
                accountDto.setAccountId((int) rs.getLong(1));
                }
        } catch (SQLException e1) {
            e1.printStackTrace();
            throw new RuntimeException(e1);
        }
        return null;
    }

    @Override
    public AccountDto update(AccountDto account) {
        return null;
    }

    @Override
    public boolean delete(Integer id) {
        return false;
    }

    public List<Account> getListOfUserAccounts(Integer userId) {
        List<Account> accounts = new ArrayList<>();
        Account account = null;
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement psm = connection.prepareStatement("select * from accounts where user_id = ?");
            psm.setInt(1, userId);
            ResultSet resultSet = psm.executeQuery();
            while (resultSet.next()) {
                account = new Account();
                account.setAccountId(resultSet.getInt("account_id"));
                account.setAccountName(resultSet.getString("account_name"));
                account.setBalance(resultSet.getInt("balance"));
                accounts.add(account);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return accounts;
    }

    @Override
    public List<AccountDto> findByAll() {
        return null;
    }
}
