package ru.metelizza.moremorey.dao;

import ru.metelizza.moremorey.service.dto.BankAccountDto;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BankAccountDao implements Dao<BankAccountDto, Integer> {

    private final DataSource dataSource;

    public BankAccountDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public BankAccountDto findById(Integer integer) {
        return null;
    }

    @Override
    public List<BankAccountDto> findByAll() {
        return null;
    }

    @Override
    public BankAccountDto insert(BankAccountDto bankAccountDto) {
        DataSource dataSource = DataSourceHikari.getDataSource();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement psm = connection.prepareStatement("insert into account_types (description) " +
                    "values (?)", Statement.RETURN_GENERATED_KEYS);
            psm.setString(1, bankAccountDto.getDescription());
            psm.execute();
            ResultSet rs = psm.getGeneratedKeys();
            if (rs.next()) {
                bankAccountDto.setId((int) rs.getLong(1));
            }
        } catch (SQLException e1) {
            e1.printStackTrace();
            throw new RuntimeException(e1);
        }

        return bankAccountDto;
    }

    @Override
    public BankAccountDto update(BankAccountDto bankAccountDto) {
        return null;
    }


    @Override
    public boolean delete(Integer integer) {
        return false;
    }

    public static List<BankAccountDto> getListOfBankAccountTypes() {
        List<BankAccountDto> accounts = new ArrayList<>();
        BankAccountDto bankAccountDto = null;
        DataSource dataSource = DataSourceHikari.getDataSource();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement psm = connection.prepareStatement("select * from account_types");
            ResultSet resultSet = psm.executeQuery();
            while (resultSet.next()) {
                bankAccountDto = new BankAccountDto();
                bankAccountDto.setId(resultSet.getInt("account_types"));
                bankAccountDto.setDescription(resultSet.getString("description"));
                accounts.add(bankAccountDto);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return accounts;
    }
}
